#-------------------------------------------------
#
# Project created by QtCreator 2018-09-10T16:07:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CameraRotation
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        camera.cpp

HEADERS  += mainwindow.h \
    camera.h

FORMS    += mainwindow.ui

RESOURCES += \
    res/resources.qrc

OTHER_FILES += \
    res/room.jpg \
    res/camera.png
