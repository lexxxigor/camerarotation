#ifndef CAMERA_H
#define CAMERA_H

#include <QGraphicsScene>
#include <QGraphicsObject>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QDebug>
#include <math.h>

#define CAMERA_HEIGHT 20
#define CAMERA_WIDTH 20

class Camera : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit Camera(QRectF rect, QString img, QString title, qreal visibilityAngle,
                    qreal visibilityRange, QGraphicsObject *parent = 0);
    ~Camera();

    QRectF boundingRect() const;
    QString getTitle() const;
    qreal getVisibilityAngle() const;
    qreal getVisibilityRange() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
    void paintSelection(QPainter *painter);
    void setTitle(QString title);
    void setVisibilityAngle(qreal angle);
    void setVisibilityRange(qreal range);

signals:
    void cameraSelected(Camera *camera);
    void cameraUnselected(Camera *camera);

public slots:
    void activateSelection();
    void clearSelection();

private:
    bool isSelected, isMovingUnselectable;
    QRectF boundRect;
    QPixmap pixmap;
    QBrush brush, selBrush, stockBrush, hoverBrush;
    QPen pen, selPen, stockPen, hoverPen;
    QString title;
    qreal visibilityAngle, visibilityRange;

    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
};

#endif // CAMERA_H
