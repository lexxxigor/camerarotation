#include "camera.h"

// Опережающее объявление
qreal calculateAngle(QPointF, QPointF, QPointF);

// Конструктор класса камеры
Camera::Camera(QRectF rect, QString img, QString title, qreal visibilityAngle,
               qreal visibilityRange, QGraphicsObject *parent) :
    QGraphicsObject(parent)
{
    boundRect = rect;
    boundRect.moveCenter(boundRect.topLeft());
    this->title = title;
    this->visibilityAngle = visibilityAngle;
    this->visibilityRange = visibilityRange;

    isSelected = false;
    isMovingUnselectable = false;

    pixmap = QPixmap(img);

    if(pixmap.height() > pixmap.width()){
        pixmap = pixmap.scaledToHeight((int)rect.height());
        if(pixmap.width() > rect.width())
            pixmap = pixmap.scaledToWidth((int)rect.width());
    }
    else {
        pixmap = pixmap.scaledToWidth((int)rect.width());
        if(pixmap.height() > rect.height())
            pixmap = pixmap.scaledToHeight((int)rect.height());
    }

    setFlags(QGraphicsItem::ItemIsMovable);
    setAcceptHoverEvents(true);

    stockBrush = QBrush(Qt::yellow);
    stockPen = QPen(Qt::black);

    selBrush = QBrush(Qt::green);
    selPen = QPen(Qt::darkGreen);

    hoverBrush = QBrush(QColor("#ffffaa"));
    hoverPen = QPen(Qt::darkGray);

    brush = stockBrush;
    pen = stockPen;
    setZValue(0);
}

// Деструктор класса камеры
Camera::~Camera(){
}

// Метод, возвращающий занимаемую камерой область
QRectF Camera::boundingRect() const {
    return boundRect;
}

// Метод, возвращающий название камеры
QString Camera::getTitle() const {
    return title;
}

// Метод, возвращающий угол обзора камеры
qreal Camera::getVisibilityAngle() const {
    return visibilityAngle;
}

// Метод, возвращающий дальность обзора камеры
qreal Camera::getVisibilityRange() const {
    return visibilityRange;
}

// Метод отрисовки камеры на сцене
void Camera::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *){
    QRectF rect = boundingRect();

    if(isSelected) paintSelection(painter);

    painter->setBrush(brush);
    painter->setPen(pen);
    painter->drawEllipse(rect.center(), rect.width() / 1.5, rect.height() / 1.5);
    painter->drawPixmap(rect.toRect(), pixmap);
}

// Метод отрисовки выделения камеры
void Camera::paintSelection(QPainter *painter){
    painter->setOpacity(0.5);
    painter->setBrush(Qt::green);
    painter->setPen(Qt::darkGreen);

    QRectF visibilityRect = QRectF(0,0, boundRect.width()*visibilityRange,
                                   boundRect.height()*visibilityRange);
    visibilityRect.moveTo(boundRect.center()-visibilityRect.center());

    int startAngle = -visibilityAngle/2 * 16;
    int spanAngle = visibilityAngle * 16;
    painter->drawPie(visibilityRect, startAngle, spanAngle);
    painter->setOpacity(1);
}

// Метод изменения названия камеры
void Camera::setTitle(QString title){
    this->title = title;
}

// Метод изменения угла обзора камеры
void Camera::setVisibilityAngle(qreal angle){
    visibilityAngle = angle;
}

// Метод изменения дальности обзора камеры
void Camera::setVisibilityRange(qreal range){
    visibilityRange = range;
}

// Метод-слот обработки сигнала выделения камеры
void Camera::activateSelection(){
    isSelected = true;
    setZValue(10);
    brush = selBrush;
    pen = selPen;
    emit cameraSelected(this);
    scene()->update();
}

// Метод-слот обработки сигнала снятия выделения камеры
void Camera::clearSelection(){
    isSelected = false;
    setZValue(0);
    brush = stockBrush;
    pen = stockPen;
    emit cameraUnselected(this);
    scene()->update();
}

// Метод обработки события наведения указателя мыши на камеру
void Camera::hoverEnterEvent(QGraphicsSceneHoverEvent *event){
    if(!isSelected){
        brush = hoverBrush;
        pen = hoverPen;
        scene()->update();
    }
    QGraphicsObject::hoverEnterEvent(event);
}

// Метод обработки события выхода указателя мыши за границы области камеры
void Camera::hoverLeaveEvent(QGraphicsSceneHoverEvent *event){
    if(!isSelected){
        brush = stockBrush;
        pen = stockPen;
        scene()->update();
    }
    QGraphicsObject::hoverLeaveEvent(event);
}

// Метод обработки события нажатия клавиши мыши на камеру
void Camera::mouseReleaseEvent(QGraphicsSceneMouseEvent *event){
    if(isMovingUnselectable || event->button() == Qt::RightButton){
        isMovingUnselectable = false;
        QGraphicsObject::mouseReleaseEvent(event);
        return;
    }

    isSelected = !isSelected;

    if(isSelected) activateSelection();
    else clearSelection();

    scene()->update();
    QGraphicsObject::mouseReleaseEvent(event);
}

// Метод обработки события движения указателя с нажатой на камеру клавишей мыши
void Camera::mouseMoveEvent(QGraphicsSceneMouseEvent *event){
    if(isSelected){
        if(event->buttons() == Qt::RightButton){
            qreal angle = calculateAngle(event->lastPos(), boundingRect().center(), event->pos());
            setRotation(rotation() + angle);
        }
        QGraphicsObject::mouseMoveEvent(event);
    }
    else
        event->ignore();

    isMovingUnselectable = true;
    scene()->update();
}

// ------------------------------------------------------------------------------------------------
// Функция вычисления угла в градусах между двумя векторами по трем точкам
qreal calculateAngle (QPointF a, QPointF o, QPointF b){
    qreal t1, t2;
    t1 = (a.x() - o.x())*(b.y() - o.y()) - (b.x()-o.x())*(a.y()-o.y());
    t2 = (a.x()-o.x())*(b.x()-o.x()) + (a.y()-o.y())*(b.y()-o.y());
    return atan2(t1, t2)*180/M_PI;
}
