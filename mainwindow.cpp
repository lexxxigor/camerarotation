#include "mainwindow.h"
#include "ui_mainwindow.h"

// Конструктор главного окна приложения
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(size());
    qsrand(QTime::currentTime().msec());

    scene = new QGraphicsScene;
    ui->cameraView->setScene(scene);
    ui->cameraView->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    ui->cameraView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    bgnItem = NULL;
    currentCamera = NULL;
    ui->removeCameraButton->setDisabled(true);
    ui->settingsBox->setDisabled(true);

    QTimer *timer = new QTimer;
    timer->setSingleShot(true);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateSceneRect()));
    connect(timer, SIGNAL(timeout()), timer, SLOT(deleteLater()));
    timer->start(1);
}

// Деструктор главного окна приложения
MainWindow::~MainWindow(){
    foreach (Camera *camera, cameras) delete camera;
    foreach (QGraphicsItem *item, scene->items()) delete item;
    delete ui;
}

// Метод-слот обновления размеров сцены в соотвествии с размерами виджета
void MainWindow::updateSceneRect(){
    QRect viewRect = ui->cameraView->viewport()->rect();

    if(scene){
        scene->setSceneRect(viewRect);
        if(bgnItem) delete bgnItem;
        bgnItem = scene->addPixmap(QPixmap(":/room").scaledToHeight(scene->sceneRect().height()));
        bgnItem->setZValue(-5);
    }
}

// Метод-слот обработки сигнала нажатия на кнопку добавления камеры
void MainWindow::on_addCameraButton_clicked(){
    static uint id = 1;
    QString cameraTitle = QString("Камера %1").arg(id++);

    Camera *camera = new Camera(QRectF(0,0,CAMERA_HEIGHT,CAMERA_WIDTH),
                                ":/camera", cameraTitle, 120, 16);
    connect(camera, SIGNAL(cameraSelected(Camera*)),
            this, SLOT(onCameraSelected(Camera*)));
    connect(camera, SIGNAL(cameraUnselected(Camera*)),
            this, SLOT(onCameraUnselected(Camera*)));

    QPointF cameraPos(CAMERA_WIDTH+qrand()%(int)(scene->sceneRect().width() - 2*CAMERA_WIDTH),
                      CAMERA_HEIGHT+qrand()%(int)(scene->sceneRect().height() - 2*CAMERA_HEIGHT));
    camera->setPos(cameraPos);

    scene->addItem(camera);
    cameras << camera;
}

// Метод-слот обработки сигнала нажатия на кнопку удаления камеры
void MainWindow::on_removeCameraButton_clicked(){
    Camera *cameraForRemove = currentCamera;
    cameras.removeAll(cameraForRemove);
    onCameraUnselected(cameraForRemove);
    delete cameraForRemove;
}

// Метод-слот обработки сигнала нажатия на кнопку применения свойств для выделенной камеры
void MainWindow::on_applyButton_clicked(){
    if(!currentCamera) return;

    if(ui->title->text().count()){
        currentCamera->setTitle(ui->title->text());
        currentCamera->setVisibilityAngle(ui->visibilityAngle->value());
        currentCamera->setVisibilityRange(ui->visibilityRange->value());
        scene->update();
    } else {
        QMessageBox::warning(this, "Внимание", "Название камеры не может быть пустым");
        return;
    }
}

// Метод-слот обработки сигнала выделения камеры на сцене
void MainWindow::onCameraSelected(Camera *camera){
    if(!camera) return;
    if(currentCamera) currentCamera->clearSelection();

    currentCamera = camera;
    ui->title->setText(camera->getTitle());
    ui->visibilityAngle->setValue(camera->getVisibilityAngle());
    ui->visibilityRange->setValue(camera->getVisibilityRange());
    ui->settingsBox->setEnabled(true);
    ui->removeCameraButton->setEnabled(true);
}

// Метод-слот обработки сигнала снятия выделения камеры на сцене
void MainWindow::onCameraUnselected(Camera*){
    currentCamera = NULL;
    ui->settingsBox->setDisabled(true);
    ui->removeCameraButton->setDisabled(true);
}
