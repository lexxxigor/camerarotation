#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QModelIndex>
#include <QTimer>
#include <QTime>
#include <QResizeEvent>
#include <QMessageBox>
#include "camera.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void updateSceneRect();
    void on_addCameraButton_clicked();
    void on_removeCameraButton_clicked();
    void on_applyButton_clicked();
    void onCameraSelected(Camera *camera);
    void onCameraUnselected(Camera*);

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QGraphicsPixmapItem *bgnItem;
    QList<Camera*> cameras;
    Camera *currentCamera;
};

#endif // MAINWINDOW_H
